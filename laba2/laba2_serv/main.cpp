#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <termios.h>
#include <cstdint>

using namespace std;

bool receiveSome(int from, /*const*/ char* data, size_t size)
{
    int received = 0;
    int bytes_received = 0;

    cout << "\nReceiving started!" << endl;
    cout << "Received: " << bytes_received << endl;
    cout << "Bytes to receive: " << size << endl << endl;

    while (bytes_received != size)
    {
        received = recv(from, /*(char*)*/data, size-bytes_received, 0);
        if (received == -1)
        {
            printf("Error %d:%s\n" ,errno,strerror(errno));
        }
        bytes_received += received;
        cout << "Received total: " << bytes_received << endl;
        cout << "Left: " << size - bytes_received << endl;
    }
    return true;
}

bool sendSome(int to, char* data, size_t size)
{
    int bytes_send = 0;
    while(bytes_send != size)
    {
        bytes_send = send(to, data, size, 0);
        if (bytes_send == -1)
        {
            printf("Error %d:%s\n" ,errno,strerror(errno));
            return false;
        }
        cout << "Sending: " << bytes_send << " bytes" << endl;
    }
    return true;
}

std::vector<char> readFile(const char* path)
{
    using namespace std;
    ifstream input(path, ios::in|ios::binary);
    if (!input)
    {
        throw runtime_error("File inaccessible!");
    }
    input.seekg(0, ios::end);
    auto const size = input.tellg();
    vector<char> content(size);
    input.seekg(0, ios::beg);
    input.read(content.data(), size);
    return content;
}

int main()
{
    cout << "Server!" << endl;


    int channel = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    sockaddr_in server;

    server.sin_family = AF_INET; 

    cout << "Enter Server IP: ";
    string address;
    cin >> address;
    server.sin_addr.s_addr =  inet_addr(address.c_str());



    cout << "Enter Port: ";
    unsigned short port;
    cin >> port;
    server.sin_port = htons(port);

    int result = bind(channel, (const sockaddr*)(&server), sizeof(server));
    if (result != 0)
    {
       printf("Error %d:%s\n" ,errno,strerror(errno));
        return -1;
    }

    int listen_result = listen(channel, SOMAXCONN);
    if (listen_result != 0)
    {
        printf("Error %d:%s\n" ,errno,strerror(errno));
        return -1;
    }


    while (1)
    {
        sockaddr_in peer;
        socklen_t peer_size = sizeof(peer);
        int client = accept(channel, (sockaddr*)&peer, &peer_size);

        cout << "Listener address is: " << inet_ntoa(peer.sin_addr) << endl;
        cout << "Listener port is: " << ntohs(peer.sin_port) << endl;

        while(1)
        {
            uint64_t msgSize = 0;
			
			std::cout<<"before first recieve!"<<std::endl;
            if (receiveSome(client, (char*)&msgSize, sizeof(msgSize)))
            {
				std::cout<<"after first recieve!"<<std::endl;
                cout << "Msg size is: " << msgSize << endl;
                char msgType = '\0';
				std::cout<<"before second recieve!"<<std::endl;

                if (receiveSome(client, &msgType, sizeof(msgType)))
                {
                    cout << "Msg Type is: " <<  msgType << endl;
                    if (msgType == 'Q')
                        break;

                    vector<char> msg;
                    msg.resize(msgSize+1, '\0');

					std::cout<<"before third recieve!"<<std::endl;

                    if (receiveSome(client, msg.data(), msgSize))
                    {
                        cout << "Type is: " << msgType << endl;
                        string str(msg.begin(), msg.end());
                        cout << str << endl;
                        cout << msg.data() << endl;


                        try
                        {
                            cout << "before: " << msg.data() << endl;
                            vector<char> content = readFile(msg.data());
                            uint64_t fileSize = content.size();
                            if (sendSome(client, (char*)&fileSize, sizeof(fileSize)))
                            {
                                char msgType = 'F';
                                if(sendSome(client, &msgType, sizeof(msgType)))
                                {
                                    if(sendSome(client, content.data(), fileSize))
                                    {
                                        cout << "file sending finished\n";
                                    }
                                    else
                                    {
                                        cout <<  "Wrong file sending!\n";
                                       printf("Error %d:%s\n" ,errno,strerror(errno));
                                        break;
                                    }
                                }
                                else
                                {
                                    cout << "Wrong file type sending\n";
                                   printf("Error %d:%s\n" ,errno,strerror(errno));
                                    break;

                                }
                            }
                            else
                            {
                                cout << "Wrong file size sending!\n";
                                printf("Error %d:%s\n" ,errno,strerror(errno));
                                break;
                            }
                        }
                        catch (const std::runtime_error& error)
                        {
                            //cout << error.what() << endl << endl;
                            uint64_t msgSize = strlen(error.what());
                            if (sendSome(client, (char*)&msgSize, sizeof(msgSize)))
                            {
                                char msgType = 'E';
                                if(sendSome(client, &msgType, sizeof(msgType)))
                                {
                                    if(sendSome(client, (char*)error.what(), msgSize))
                                    {
                                        cout << "Error sending finished\n";
                                    }
                                    else
                                    {
                                        cout << "Wrong error msg sending!\n";
                                        printf("Error %d:%s\n" ,errno,strerror(errno));
                                        break;
                                    }
                                }
                                else
                                {
                                    cout << "Wrong error type sending!\n";
                                    printf("Error %d:%s\n" ,errno,strerror(errno));
                                    break;
                                }
                            }
                            else
                            {
                                cout << "Wrong error size sending!\n";
                                printf("Error %d:%s\n" ,errno,strerror(errno));
                                break;
                            }
                        }
                    }
                    else
                    {
                        cout << "wrong rcv msg\n";
                            printf("Error %d:%s\n" ,errno,strerror(errno));
                        break;
                    }
                }

            }
        }
    }
    return 0;
}